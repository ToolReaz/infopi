import { Box, Tab, TabList, TabPanel, TabPanels, Tabs } from "@chakra-ui/react";
import { useState } from "react";
import Weather from "./Weather";

const BASE_URL = "https://www.flightradar24.com/simple";
const LAT = 47.59;
const LON = 7.52;
const Z = 11;

function App() {
  const [count, setCount] = useState(0);

  const cancelClick = (e) => e.preventDefault();

  return (
    <Box as="main" h="full" w="full">
      <Box
        position="absolute"
        w="20"
        h="20"
        bgColor="red"
        top={0}
        left="-50%"
        zIndex={99999}
      >
        test
      </Box>
      <Tabs h="full" size="lg" variant="enclosed" isFitted>
        <TabList className="tabs">
          <Tab>Flight Radar</Tab>
          <Tab>Weather</Tab>
          <Tab>Settings</Tab>
        </TabList>
        <TabPanels className="panels">
          <TabPanel h="full" p={0} overflow="hidden">
            <iframe
              title="FlightRadar24"
              type="text/html"
              width="140%"
              height="105%"
              style={{ transform: "translateX(-10%)" }}
              src={`${BASE_URL}?lat=${LAT}&lon=${LON}&z=${Z}`}
              frameBorder="0"
              onClick={cancelClick}
            ></iframe>
          </TabPanel>
          <TabPanel h="full" p={0}>
            <Weather cords={{ LAT, LON }} />
          </TabPanel>
          <TabPanel h="full">Settings</TabPanel>
        </TabPanels>
      </Tabs>
    </Box>
  );
}

export default App;
