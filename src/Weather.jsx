import {
  Heading,
  Spinner,
  Stat,
  StatLabel,
  StatNumber,
  StatHelpText,
  StatArrow,
  StatGroup,
} from "@chakra-ui/react";
import React, { useMemo } from "react";
import { useQuery } from "react-query";
import wretch from "wretch";

export default function Weather({ cords }) {
  const URL = useMemo(
    () =>
      `https://api.open-meteo.com/v1/meteofrance?latitude=${cords.LAT}&longitude=${cords.LON}&daily=weathercode,temperature_2m_max&current_weather=true&timezone=Europe%2FBerlin`,
    []
  );

  const { isLoading, data } = useQuery("weather", () =>
    wretch(URL).get().json()
  );

  console.log(data);

  if (isLoading || !data) {
    return <Spinner />;
  }

  return (
    <div>
      <Stat>
        <StatLabel>Temperature</StatLabel>
        <StatNumber>{data.current_weather?.temperature}°C</StatNumber>
      </Stat>
    </div>
  );
}
